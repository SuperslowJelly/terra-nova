val draci = <draconicevolution:draconium_ingot>;
val wcore = <draconicevolution:wyvern_core>;
val diamb = <minecraft:diamond_block>;
val goldb = <minecraft:gold_block>;
val dracc = <draconicevolution:draconic_core>;
val neths = <minecraft:nether_star>;
val eocap = <enderio:item_basic_capacitor:2>;
val adraci = <draconicevolution:draconic_ingot>;
val wecore = <draconicevolution:wyvern_energy_core>;
val aecore = <draconicevolution:draconic_energy_core>;
val coal = <minecraft:coal>;
val iron = <minecraft:iron_ingot>;
val bdye = <ore:dyeBlack>;
val cquar = <ore:dustNetherQuartz>;
val biron = <extendedcrafting:material:0>;
val diam = <minecraft:diamond>;
val gold = <minecraft:gold_ingot>;
val edcap = <enderio:item_basic_capacitor:1>;
val redblock = <minecraft:redstone_block>;
val oec = <overloaded:energy_core>;
val nsbloc = <ore:blockNetherStar>;
val pcap = <pneumaticcraft:capacitor>;
val ptrans = <pneumaticcraft:transistor>;
val ofc = <overloaded:fluid_core>;
val rdrum = <extrautils2:drum:2>;
val ironb = <minecraft:iron_block>;
val uchest = <ore:chest>;
val oic = <overloaded:item_core>;
val sixcell = <appliedenergistics2:storage_cell_64k>;
val cryschest = <ironchest:iron_chest:5>;
val infbar = <overloaded:infinite_barrel>;
val inftank = <overloaded:infinite_tank>;
val infcap = <overloaded:infinite_capacitor>;
val sponge = <minecraft:sponge>;
val wycap = <draconicevolution:draconium_capacitor:0>;
val drcap = <draconicevolution:draconium_capacitor:1>;
val org = <overloaded:railgun>;
val omt = <overloaded:multi_tool>;
val ohelm = <overloaded:multi_helmet>;
val ochest = <overloaded:multi_chestplate>;
val olegs = <overloaded:multi_leggings>;
val oboots = <overloaded:multi_boots>;
val adpcb = <pneumaticcraft:advanced_pcb>;
val vcap = <enderio:block_cap_bank:3>;
val lattice = <avaritia:resource>;
val matrixi = <avaritia:resource:1>;
val advancedcrafting = <extendedcrafting:table_advanced>;
val elitecrafting = <extendedcrafting:table_elite>;
val extremecrafting = <avaritia:extreme_crafting_table>;
val doublecrafting = <avaritia:double_compressed_crafting_table>;
val diahelm = <minecraft:diamond_helmet>;
val diachest = <minecraft:diamond_chestplate>;
val dialeg = <minecraft:diamond_leggings>;
val diaboot = <minecraft:diamond_boots>;
val wyvhelm = <draconicevolution:wyvern_helm>;
val wyvchest = <draconicevolution:wyvern_chest>;
val wyvleg = <draconicevolution:wyvern_legs>;
val wyvboot = <draconicevolution:wyvern_boots>;
val dracblock = <draconicevolution:draconium_block>;
val woodplank = <ore:plankWood>;
val redstone = <minecraft:redstone>;
val mirror = <solarflux:mirror>;
val piston = <minecraft:piston>;
val solar1 = <solarflux:solar_panel_1>;
val solar2 = <solarflux:solar_panel_2>;
val repeater = <minecraft:repeater>;
val mirror1 = <solarflux:photovoltaic_cell_1>;
val solar3 = <solarflux:solar_panel_3>;
val solar4 = <solarflux:solar_panel_4>;
val clock = <minecraft:clock>;
val mirror2 = <solarflux:photovoltaic_cell_2>;
val diasw = <minecraft:diamond_sword>;
val diash = <minecraft:diamond_shovel>;
val wyvsw = <draconicevolution:wyvern_sword>;
val wyvsh = <draconicevolution:wyvern_shovel>;
val diapk = <minecraft:diamond_pickaxe>;
val wyvpk = <draconicevolution:wyvern_pick>;
val diaax = <minecraft:diamond_axe>;
val wyvax = <draconicevolution:wyvern_axe>;
val decor = <draconicevolution:energy_storage_core>;
val emral = <minecraft:emerald>;
val eyeoe = <minecraft:ender_eye>;
val laplb = <minecraft:lapis_block>;
val stone = <minecraft:stone>;
val blaze = <minecraft:blaze_rod>;
val enrgp = <draconicevolution:energy_pylon>;
val fuccr = <draconicevolution:fusion_crafting_core>;
val fucin = <draconicevolution:crafting_injector>;
val pargen = <draconicevolution:particle_generator>;
val enrgcs = <draconicevolution:particle_generator:2>;
val wyvfp = <draconicevolution:draconium_capacitor>;
val awkco = <draconicevolution:awakened_core>;
val emrab = <minecraft:emerald_block>;
val irons = <avaritia:singularity>;
val golds = <avaritia:singularity:1>;
val lapiss = <avaritia:singularity:2>;
val redss = <avaritia:singularity:3>;
val nethq = <avaritia:singularity:4>;
val sing5 = <avaritia:singularity:5>;
val sing6 = <avaritia:singularity:6>;
val sing7 = <avaritia:singularity:7>;
val sing8 = <avaritia:singularity:8>;
val sing9 = <avaritia:singularity:9>;
val sing10 = <avaritia:singularity:10>;
val sing11 = <avaritia:singularity:11>;
val sing12 = <avaritia:singularity:12>;
val ultis = <avaritia:ultimate_stew>;
val cosmm = <avaritia:cosmic_meatballs>;
val endsp = <avaritia:endest_pearl>;
val recof = <avaritia:resource:7>;
val neunu = <avaritia:resource:3>;
val crymn = <avaritia:resource:1>;
val pilon = <avaritia:resource:2>;
val nueti = <avaritia:resource:4>;
val blkos = <thermalfoundation:storage_alloy>;
val endrb = <thermalfoundation:storage_alloy:7>;
val darsb = <enderio:block_alloy:6>;
val infca = <avaritia:resource:5>;
val infig = <avaritia:resource:6>;
val infigb = <avaritia:block_resource:1>;
val infhe = <avaritia:infinity_helmet>;
val infbp = <avaritia:infinity_chestplate>;
val infle = <avaritia:infinity_pants>;
val infbo = <avaritia:infinity_boots>;
val swooc = <avaritia:infinity_sword>;
val lonbh = <avaritia:infinity_bow>;
val worldb = <avaritia:infinity_pickaxe>;
val plnte = <avaritia:infinity_shovel>;
val nturr = <avaritia:infinity_axe>;
val skulf = <avaritia:skullfire_sword>;
val wools = <ore:woolBlock>;
val infho = <avaritia:infinity_hoe>;
val cryma = <avaritia:block_resource:2>;
val blzpd = <minecraft:blaze_powder>;
val bones = <minecraft:bone>;
val loggs = <ore:logWood>;
val iring = <aether_legacy:iron_ring>;
val niron = <minecraft:iron_nugget>;
val ppick = <tp:birthday_pickaxe>;
val gring = <aether_legacy:golden_ring>;
val swren = <rftools:smartwrench>;


recipes.remove(biron, false);
recipes.remove(wcore, false);
recipes.remove(aecore, false);
recipes.remove(dracc, false);
recipes.remove(wecore, false);
recipes.remove(oec, false);
recipes.remove(ofc, false);
recipes.remove(oic, false);
recipes.remove(infbar, false);
recipes.remove(infcap, false);
recipes.remove(inftank, false);
recipes.remove(org, false);
recipes.remove(omt, false);
recipes.remove(ohelm, false);
recipes.remove(ochest, false);
recipes.remove(olegs, false);
recipes.remove(oboots, false);
recipes.remove(iring, false);
recipes.remove(ppick, false);
recipes.remove(matrixi, false);
recipes.remove(gring, false);
recipes.remove(swren, false);
mods.avaritia.ExtremeCrafting.remove(infca);
recipes.remove(<littletiles:ltstorageblocktile>, false);


recipes.addShaped("Little Storage Block", <littletiles:ltstorageblocktile>,
 [[null,iron,null],
  [null,<minecraft:chest>,null],
  [null,null,null]]);

recipes.addShaped("Smart Wrench", swren,
 [[null,iron,null],
  [null,<minecraft:dye:4>,iron],
  [<minecraft:dye:4>,null,null]]);

recipes.addShaped("Golden Ring", gring,
 [[null,<minecraft:gold_nugget>,null],
  [<minecraft:gold_ingot>,null,<minecraft:gold_ingot>],
  [null,<minecraft:gold_ingot>,null]]);

recipes.addShaped("Party Pickaxe", ppick,
 [[<tp:reinforced_obsidian>,<minecraft:diamond>,<tp:reinforced_obsidian>],
  [null,iron,null],
  [null,iron,null]]);

recipes.addShaped("Crystal Matrix Ingot", matrixi,
 [[null,null,null],
  [null,<avaritia:block_resource:2>,null],
  [null,null,null]]);

mods.extendedcrafting.TableCrafting.addShaped(2, wcore, 
[[null,null,draci,null,null], 
 [null,goldb,dracc,pcap,null], 
 [draci,dracc,neths,dracc,draci], 
 [null,ptrans,dracc,diamb,null], 
 [null,null,draci,null,null]]); 

mods.extendedcrafting.TableCrafting.addShaped(2, aecore, 
[[null,null,adraci,null,null], 
 [null,eocap,wecore,eocap,null], 
 [adraci,wecore,wcore,wecore,adraci], 
 [null,eocap,wecore,eocap,null], 
 [null,null,adraci,null,null]]);

recipes.addShapeless("black_iron",biron,[coal,iron,bdye,cquar]);

mods.extendedcrafting.TableCrafting.addShaped(1, dracc,
 [[draci,gold,draci], 
 [gold,diam,gold], 
 [draci,gold,draci]]);

mods.extendedcrafting.TableCrafting.addShaped(2, wecore, 
[[null,null,draci,null,null], 
 [null,edcap,redblock,edcap,null], 
 [draci,redblock,dracc,redblock,draci], 
 [null,edcap,redblock,edcap,null], 
 [null,null,draci,null,null]]); 

mods.extendedcrafting.TableCrafting.addShaped(1, oec,
 [[pcap,nsbloc,ptrans], 
 [nsbloc,redblock,nsbloc], 
 [ptrans,nsbloc,pcap]]);

mods.extendedcrafting.TableCrafting.addShaped(1, ofc,
 [[rdrum,nsbloc,rdrum], 
 [nsbloc,ironb,nsbloc], 
 [rdrum,nsbloc,rdrum]]);

mods.extendedcrafting.TableCrafting.addShaped(1, oic,
 [[sixcell,nsbloc,cryschest], 
 [nsbloc,uchest,nsbloc], 
 [cryschest,nsbloc,sixcell]]);

mods.extendedcrafting.TableCrafting.addShaped(1, infbar,
 [[cryschest,nsbloc,cryschest], 
 [nsbloc,oic,nsbloc], 
 [cryschest,nsbloc,cryschest]]);

mods.extendedcrafting.TableCrafting.addShaped(1, inftank,
 [[sponge,nsbloc,sponge], 
 [nsbloc,ofc,nsbloc], 
 [sponge,nsbloc,sponge]]);

mods.extendedcrafting.TableCrafting.addShaped(1, infcap,
 [[pcap,nsbloc,wycap], 
 [nsbloc,oec,nsbloc], 
 [wycap,nsbloc,pcap]]);


mods.extendedcrafting.TableCrafting.addShaped(1, omt,
 [[neths,wycap,null], 
 [wycap,infcap,nsbloc], 
 [null,nsbloc,ptrans]]);

mods.extendedcrafting.TableCrafting.addShaped(1, ohelm,
 [[nsbloc,vcap,nsbloc], 
 [nsbloc,infcap,nsbloc], 
 [nsbloc,wycap,nsbloc]]);

mods.extendedcrafting.TableCrafting.addShaped(1, ochest,
 [[nsbloc,adpcb,nsbloc], 
 [nsbloc,infcap,nsbloc], 
 [vcap,nsbloc,vcap]]);

mods.extendedcrafting.TableCrafting.addShaped(1, olegs,
 [[nsbloc,nsbloc,nsbloc], 
 [nsbloc,infcap,nsbloc], 
 [nsbloc,adpcb,nsbloc]]);

mods.extendedcrafting.TableCrafting.addShaped(1, oboots,
 [[pcap,null,ptrans], 
 [nsbloc,infcap,nsbloc], 
 [adpcb,null,adpcb]]);

mods.extendedcrafting.TableCrafting.addShaped(3, org, 
[	
	[<ore:blockNetherStar>, <ore:blockNetherStar>, <ore:blockNetherStar>, <ore:blockNetherStar>, <pneumaticcraft:advanced_pcb>, <ore:blockNetherStar>, null], 
	[<enderio:item_basic_capacitor:2>, <enderio:item_basic_capacitor:2>, <enderio:item_basic_capacitor:2>, <enderio:item_basic_capacitor:2>, <overloaded:infinite_capacitor>, <ore:blockNetherStar>, <ore:blockNetherStar>], 
	[null, null, null, null, null, <draconicevolution:draconium_capacitor:1>, <ore:blockNetherStar>]
	
]);



recipes.remove(extremecrafting, false);
recipes.remove(wyvhelm, false);
recipes.remove(wyvchest, false);
recipes.remove(wyvleg, false);
recipes.remove(wyvboot, false);
recipes.remove(solar1, false);
recipes.remove(solar2, false);
recipes.remove(solar3, false);
recipes.remove(solar4, false);
recipes.remove(<solarflux:solar_panel_5>, false);
recipes.remove(<solarflux:solar_panel_6>, false);
recipes.remove(<solarflux:solar_panel_7>, false);
recipes.remove(<solarflux:solar_panel_8>, false);
recipes.remove(<solarflux:solar_panel_wyvern>, false);
recipes.remove(<solarflux:solar_panel_draconic>, false);
recipes.remove(<mysticalagriculture:crafting:16>, false);
recipes.remove(<mysticalagradditions:nether_star_seeds>, false);
recipes.remove(<mysticalagradditions:neutronium_seeds>, false);
recipes.remove(<mysticalagriculture:draconium_seeds>, false);
recipes.remove(<mysticalagriculture:wither_skeleton_seeds>, false);
recipes.remove(<mysticalagradditions:dragon_egg_seeds>, false);
recipes.remove(<mysticalagradditions:awakened_draconium_seeds>, false);
recipes.remove(<tp:emerald_helmet>, false);
recipes.remove(<tp:emerald_chestplate>, false);
recipes.remove(<tp:emerald_leggings>, false);
recipes.remove(<tp:emerald_boots>, false);
recipes.remove(<tp:emerald_pickaxe>, false);
recipes.remove(<tp:emerald_sword>, false);
recipes.remove(<tp:emerald_hoe>, false);
recipes.remove(<tp:emerald_spade>, false);
recipes.remove(<tp:emerald_axe>, false);
recipes.remove(<tp:obsidian_helmet>, false);
recipes.remove(<tp:obsidian_chestplate>, false);
recipes.remove(<tp:obsidian_leggings>, false);
recipes.remove(<tp:obsidian_boots>, false);
recipes.remove(<tp:obsidian_pickaxe>, false);
recipes.remove(<tp:obsidian_sword>, false);
recipes.remove(<tp:obsidian_hoe>, false);
recipes.remove(<tp:obsidian_spade>, false);
recipes.remove(<tp:obsidian_axe>, false);
recipes.remove(<cyclicmagic:emerald_chestplate>, false);
recipes.remove(<cyclicmagic:emerald_leggings>, false);
recipes.remove(<cyclicmagic:emerald_boots>, false);
recipes.remove(<cyclicmagic:emerald_helmet>, false);
recipes.remove(<cyclicmagic:emerald_spade>, false);
recipes.remove(<cyclicmagic:emerald_axe>, false);
recipes.remove(<cyclicmagic:emerald_hoe>, false);
recipes.remove(<cyclicmagic:emerald_pickaxe>, false);
recipes.remove(<cyclicmagic:emerald_sword>, false);





mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagradditions:awakened_draconium_seeds>, [
	[<ore:nitor>, <ore:nitor>, <draconicevolution:draconic_core>, <draconicevolution:wyvern_core>, <draconicevolution:draconic_core>, <ore:nitor>, <ore:nitor>], 
	[<ore:nitor>, <draconicevolution:draconic_core>, <ore:ingotDraconiumAwakened>, <draconicevolution:dragon_heart>, <ore:ingotDraconiumAwakened>, <draconicevolution:draconic_core>, <ore:nitor>], 
	[<draconicevolution:draconic_core>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <ore:essenceInsanium>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <draconicevolution:draconic_core>], 
	[<draconicevolution:wyvern_core>, <draconicevolution:dragon_heart>, <ore:essenceInsanium>, <mysticalagradditions:insanium:1>, <ore:essenceInsanium>, <draconicevolution:dragon_heart>, <draconicevolution:wyvern_core>], 
	[<draconicevolution:draconic_core>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <ore:essenceInsanium>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <draconicevolution:draconic_core>], 
	[<ore:nitor>, <draconicevolution:draconic_core>, <ore:ingotDraconiumAwakened>, <draconicevolution:dragon_heart>, <ore:ingotDraconiumAwakened>, <draconicevolution:draconic_core>, <ore:nitor>], 
	[<ore:nitor>, <ore:nitor>, <draconicevolution:draconic_core>, <draconicevolution:wyvern_core>, <draconicevolution:draconic_core>, <ore:nitor>, <ore:nitor>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagradditions:dragon_egg_seeds>, [
	[<minecraft:end_crystal>, <ore:dragonEgg>, <iceandfire:ice_dragon_heart>, <ore:itemSkull>, <iceandfire:fire_dragon_heart>, <ore:dragonEgg>, <minecraft:end_crystal>], 
	[<ore:nuggetCosmicNeutronium>, <ore:endstone>, <ore:endstone>, <draconicevolution:dragon_heart>, <ore:endstone>, <ore:endstone>, <ore:nuggetCosmicNeutronium>], 
	[<ore:nuggetCosmicNeutronium>, <ore:endstone>, <mysticalagradditions:stuff:3>, <ore:essenceInsanium>, <mysticalagradditions:stuff:3>, <ore:endstone>, <ore:nuggetCosmicNeutronium>], 
	[<ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:essenceInsanium>, <mysticalagradditions:insanium:1>, <ore:essenceInsanium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>], 
	[<ore:nuggetCosmicNeutronium>, <ore:endstone>, <mysticalagradditions:stuff:3>, <ore:essenceInsanium>, <mysticalagradditions:stuff:3>, <ore:endstone>, <ore:nuggetCosmicNeutronium>], 
	[<ore:nuggetCosmicNeutronium>, <ore:endstone>, <ore:endstone>, <draconicevolution:ender_energy_manipulator>, <ore:endstone>, <ore:endstone>, <ore:nuggetCosmicNeutronium>], 
	[<minecraft:end_crystal>, <ore:enderpearl>, <ore:enderpearl>, <avaritia:endest_pearl>, <ore:enderpearl>, <ore:enderpearl>, <minecraft:end_crystal>]
]);


mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:wither_skeleton_seeds>, [
	[<ore:itemSkull>, <avaritia:resource:2>, <ore:boneWithered>, <avaritia:resource:2>, <ore:itemSkull>], 
	[<avaritia:resource:2>, <mysticalagriculture:chunk:20>, <ore:essenceSupremium>, <mysticalagriculture:chunk:20>, <avaritia:resource:2>], 
	[<ore:boneWithered>, <ore:essenceSupremium>, <mysticalagriculture:crafting:21>, <ore:essenceSupremium>, <ore:boneWithered>], 
	[<avaritia:resource:2>, <mysticalagriculture:chunk:20>, <ore:essenceSupremium>, <mysticalagriculture:chunk:20>, <avaritia:resource:2>], 
	[<ore:itemSkull>, <avaritia:resource:2>, <ore:boneWithered>, <avaritia:resource:2>, <ore:itemSkull>]
]);


mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:draconium_seeds>, [
	[<ore:ingotDraconium>, <ore:enderpearl>, <ore:ingotDraconium>, <ore:enderpearl>, <ore:ingotDraconium>], 
	[<ore:enderpearl>, <ore:essenceSupremium>, <draconicevolution:dragon_heart>, <ore:essenceSupremium>, <ore:enderpearl>], 
	[<ore:ingotDraconium>, <ore:enderpearl>, <mysticalagriculture:crafting:21>, <ore:enderpearl>, <ore:ingotDraconium>], 
	[<ore:enderpearl>, <ore:essenceSupremium>, <ore:dragonEgg>, <ore:essenceSupremium>, <ore:enderpearl>], 
	[<ore:ingotDraconium>, <ore:enderpearl>, <ore:ingotDraconium>, <ore:enderpearl>, <ore:ingotDraconium>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagradditions:neutronium_seeds>, [
	[<nuclearcraft:fusion_electromagnet_idle>, <nuclearcraft:fusion_electromagnet_idle>, <nuclearcraft:fusion_electromagnet_idle>, <nuclearcraft:fusion_electromagnet_idle>, <nuclearcraft:fusion_electromagnet_idle>, <nuclearcraft:fusion_electromagnet_idle>, <nuclearcraft:fusion_electromagnet_idle>], 
	[<nuclearcraft:fusion_electromagnet_idle>, <ore:compressed4xCobblestone>, <ore:nuggetCosmicNeutronium>, <avaritia:neutronium_compressor>, <ore:nuggetCosmicNeutronium>, <ore:compressed4xCobblestone>, <nuclearcraft:fusion_electromagnet_idle>], 
	[<nuclearcraft:fusion_electromagnet_idle>, <ore:nuggetCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:essenceInsanium>, <ore:ingotCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <nuclearcraft:fusion_electromagnet_idle>], 
	[<nuclearcraft:fusion_electromagnet_idle>, <ore:nuggetCosmicNeutronium>, <ore:essenceInsanium>, <mysticalagradditions:insanium:1>, <ore:essenceInsanium>, <ore:nuggetCosmicNeutronium>, <nuclearcraft:fusion_electromagnet_idle>], 
	[<nuclearcraft:fusion_electromagnet_idle>, <ore:nuggetCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:essenceInsanium>, <ore:ingotCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <nuclearcraft:fusion_electromagnet_idle>], 
	[<nuclearcraft:fusion_electromagnet_idle>, <ore:compressed4xCobblestone>, <ore:nuggetCosmicNeutronium>, <avaritia:neutron_collector>, <ore:nuggetCosmicNeutronium>, <ore:compressed4xCobblestone>, <nuclearcraft:fusion_electromagnet_idle>], 
	[<nuclearcraft:fusion_electromagnet_idle>, <nuclearcraft:fusion_electromagnet_idle>, <nuclearcraft:fusion_electromagnet_idle>, <nuclearcraft:fusion_electromagnet_idle>, <nuclearcraft:fusion_electromagnet_idle>, <nuclearcraft:fusion_electromagnet_idle>, <nuclearcraft:fusion_electromagnet_idle>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagradditions:nether_star_seeds>, [
	[<woot:shard:3>, <woot:shard:3>, <ore:itemSkull>, <ore:itemSkull>, <ore:itemSkull>, <woot:shard:3>, <woot:shard:3>], 
	[<woot:shard:3>, <woot:shard:3>, <ore:soulSand>, <ore:soulSand>, <ore:soulSand>, <woot:shard:3>, <woot:shard:3>], 
	[<woot:shard:3>, <woot:shard:3>, <mob_grinding_utils:saw>, <ore:soulSand>, <mob_grinding_utils:saw>, <woot:shard:3>, <woot:shard:3>], 
	[<woot:shard:3>, <ore:essenceInsanium>, <ore:essenceInsanium>, <mysticalagradditions:insanium:1>, <ore:essenceInsanium>, <ore:essenceInsanium>, <woot:shard:3>], 
	[<ore:netherStar>, <ore:nuggetCosmicNeutronium>, <ore:netherStar>, <ore:essenceInsanium>, <ore:netherStar>, <ore:nuggetCosmicNeutronium>, <ore:netherStar>], 
	[<ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>], 
	[<ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:crafting:16>, [
	[null, <ore:shardProsperity>, null], 
	[<ore:shardProsperity>, <botania:grassseeds>, <ore:shardProsperity>], 
	[null, <ore:shardProsperity>, null]
]);

mods.extendedcrafting.TableCrafting.addShaped(matrixi,
[
 [diam,diam,lattice,diam,diam],
 [diam,lattice,neths,lattice,diam],
 [diam,diam,lattice,diam,diam]
 ]);
 
mods.extendedcrafting.TableCrafting.addShaped(extremecrafting,
[[matrixi,matrixi,matrixi,matrixi,matrixi,matrixi,matrixi],
 [matrixi,diamb,doublecrafting,diamb,doublecrafting,diamb,matrixi],
 [matrixi,doublecrafting,doublecrafting,advancedcrafting,doublecrafting,doublecrafting,matrixi],
 [matrixi,diamb,advancedcrafting,elitecrafting,advancedcrafting,diamb,matrixi],
 [matrixi,doublecrafting,doublecrafting,advancedcrafting,doublecrafting,doublecrafting,matrixi],
 [matrixi,diamb,doublecrafting,diamb,doublecrafting,diamb,matrixi],
 [matrixi,matrixi,matrixi,matrixi,matrixi,matrixi,matrixi]]);
 
mods.extendedcrafting.TableCrafting.addShaped(wyvhelm,
[[draci, wcore, draci],
 [dracblock, diahelm, dracblock],
 [draci, wecore, draci]]);
 
mods.extendedcrafting.TableCrafting.addShaped(wyvchest,
[[draci, wcore, draci],
 [dracblock, diachest, dracblock],
 [draci, wecore, draci]]);
 
mods.extendedcrafting.TableCrafting.addShaped(wyvleg,
[[draci, wcore, draci],
 [dracblock, dialeg, dracblock],
 [draci, wecore, draci]]);
 
mods.extendedcrafting.TableCrafting.addShaped(wyvboot,
[[draci, wcore, draci],
 [dracblock, diaboot, dracblock],
 [draci, wecore, draci]]);
 
mods.extendedcrafting.TableCrafting.addShaped(solar1 * 2,
[[mirror, mirror, mirror],
 [woodplank, redstone, woodplank],
 [woodplank, woodplank, woodplank]]);
 
mods.extendedcrafting.TableCrafting.addShaped(solar2 * 2,
[[solar1, solar1,solar1],
 [solar1, piston, solar1],
 [solar1, solar1, solar1]]);
 
mods.extendedcrafting.TableCrafting.addShaped(solar3 * 2,
[[mirror1, mirror1, mirror1],
 [solar2, repeater, solar2],
 [solar2, ironb, solar2]]);
 
mods.extendedcrafting.TableCrafting.addShaped(solar4 * 2,
[[mirror2, mirror2, mirror2],
 [solar3, clock, solar3],
 [solar3, ironb, solar3]]);
 
 mods.extendedcrafting.TableCrafting.addShaped(<solarflux:solar_panel_5> * 2,
 [[<solarflux:photovoltaic_cell_3>, <solarflux:photovoltaic_cell_3>, <solarflux:photovoltaic_cell_3>], 
	[<solarflux:solar_panel_4>, <ore:dustGlowstone>, <solarflux:solar_panel_4>], 
	[<solarflux:solar_panel_4>, <ore:blockGold>, <solarflux:solar_panel_4>]]);
 
mods.extendedcrafting.TableCrafting.addShaped(<solarflux:solar_panel_6> * 2,
 [[<solarflux:photovoltaic_cell_4>, <solarflux:photovoltaic_cell_4>, <solarflux:photovoltaic_cell_4>], 
	[<solarflux:solar_panel_5>, <minecraft:redstone_lamp>, <solarflux:solar_panel_5>], 
	[<solarflux:solar_panel_5>, <ore:blockDiamond>, <solarflux:solar_panel_5>]]);
	
mods.extendedcrafting.TableCrafting.addShaped(<solarflux:solar_panel_7> * 2,[
	[<solarflux:photovoltaic_cell_5>, <solarflux:photovoltaic_cell_5>, <solarflux:photovoltaic_cell_5>], 
	[<solarflux:solar_panel_6>, <minecraft:dragon_breath>, <solarflux:solar_panel_6>], 
	[<solarflux:solar_panel_6>, <minecraft:dragon_breath>, <solarflux:solar_panel_6>]
]);	
 
mods.extendedcrafting.TableCrafting.addShaped(<solarflux:solar_panel_8> * 2,[
	[<solarflux:photovoltaic_cell_6>, <solarflux:photovoltaic_cell_6>, <solarflux:photovoltaic_cell_6>], 
	[<solarflux:solar_panel_7>, <ore:dragonEgg>, <solarflux:solar_panel_7>], 
	[<solarflux:solar_panel_7>, <ore:dragonEgg>, <solarflux:solar_panel_7>]
]); 
 
mods.extendedcrafting.TableCrafting.addShaped(<solarflux:solar_panel_wyvern> * 2,[
	[<solarflux:solar_panel_8>, <draconicevolution:wyvern_energy_core>, <solarflux:solar_panel_8>], 
	[<draconicevolution:wyvern_energy_core>, <draconicevolution:wyvern_core>, <draconicevolution:wyvern_energy_core>], 
	[<solarflux:solar_panel_8>, <draconicevolution:wyvern_energy_core>, <solarflux:solar_panel_8>]
]); 

mods.extendedcrafting.TableCrafting.addShaped(<solarflux:solar_panel_draconic> * 2,[
	[<solarflux:solar_panel_wyvern>, <draconicevolution:draconic_energy_core>, <solarflux:solar_panel_wyvern>], 
	[<draconicevolution:draconic_energy_core>, <draconicevolution:awakened_core>, <draconicevolution:draconic_energy_core>], 
	[<solarflux:solar_panel_wyvern>, <draconicevolution:draconic_energy_core>, <solarflux:solar_panel_wyvern>]
]); 
 
 
 
//Wyvern Sword
recipes.remove(wyvsw, false);


mods.extendedcrafting.TableCrafting.addShaped(wyvsw,
[[diam,wcore,diam],
 [draci,diasw,draci],
 [diam,wecore,diam]]);

//Wyvern Shovel
recipes.remove(wyvsh, false);


mods.extendedcrafting.TableCrafting.addShaped(wyvsh,
[[diam,wcore,diam],
 [draci,diash,draci],
 [diam,wecore,diam]]);

//Wyvern Pick
recipes.remove(wyvpk, false);


mods.extendedcrafting.TableCrafting.addShaped(wyvpk,
[[diam,wcore,diam],
 [draci,diapk,draci],
 [diam,wecore,diam]]);

//Wyvern Axe
recipes.remove(wyvax, false);


mods.extendedcrafting.TableCrafting.addShaped(wyvax,
[[diam,wcore,diam],
 [draci,diaax,draci],
 [diam,wecore,diam]]);

//Draconic Energy Core
recipes.remove(decor, false);


mods.extendedcrafting.TableCrafting.addShaped(decor,
[[draci,draci,draci],
 [wecore,wcore,wecore],
 [draci,draci,draci]]);

//Energy Pylon
recipes.remove(enrgp, false);


mods.extendedcrafting.TableCrafting.addShaped(enrgp,
[[draci,eyeoe,draci],
 [emral,dracc,emral],
 [draci,diam,draci]]);


//Fusion Crafting Core
recipes.remove(fuccr, false);


mods.extendedcrafting.TableCrafting.addShaped(fuccr,
[[laplb,diam,laplb],
 [diam,dracc,diam],
 [laplb,diam,laplb]]);


//Fusion Crafting Injectors
recipes.remove(fucin, false);


mods.extendedcrafting.TableCrafting.addShaped(fucin,
[[diam,dracc,diam],
 [stone,ironb,stone],
 [stone,stone,stone]]);


//Particle Generator
recipes.remove(pargen, false);


mods.extendedcrafting.TableCrafting.addShaped(pargen,
[[redblock,blaze,redblock],
 [blaze,dracc,blaze],
 [redblock,blaze,redblock]]);


//Energy Core Stabilizer
recipes.remove(enrgcs, false);


mods.extendedcrafting.TableCrafting.addShaped(enrgcs,
[[diam,null,diam],
 [null,pargen,null],
 [diam,null,diam]]);



//Wyvern Flux Capacitor
recipes.remove(wyvfp, false);


mods.extendedcrafting.TableCrafting.addShaped(wyvfp,
[[draci,wecore,draci],
 [wecore,wcore,wecore],
 [draci,wecore,draci]]);



//Draconic Flux Capacitor
recipes.remove(drcap, false);


mods.extendedcrafting.TableCrafting.addShaped(drcap,
[[adraci,awkco,adraci],
 [aecore,wyvfp,aecore],
 [adraci,aecore,adraci]]);


//Infinity Catalyst
mods.extendedcrafting.TableCrafting.addShaped(0, <avaritia:resource:5>, [
	[<ore:ingotExtreme>, <ore:ingotExtreme>, <actuallyadditions:item_crystal_empowered>, <ore:blockEnderium>, <actuallyadditions:item_crystal_empowered:4>, <ore:blockElectrumFlux>, <actuallyadditions:item_crystal_empowered>, <ore:ingotExtreme>, <ore:ingotExtreme>], 
	[<ore:ingotExtreme>, <ore:blockCosmicNeutronium>, <ore:gemBoronNitride>, <ore:blockEnderium>, <extrautils2:suncrystal>, <ore:blockElectrumFlux>, <ore:gemBoronNitride>, <ore:blockCosmicNeutronium>, <ore:ingotExtreme>], 
	[<actuallyadditions:item_crystal_empowered:3>, <ore:gemBoronNitride>, <ore:gemBoronNitride>, <ore:blockOsgloglas>, <ore:eternalLifeEssence>, <ore:blockMirion>, <ore:gemBoronNitride>, <ore:gemBoronNitride>, <actuallyadditions:item_crystal_empowered:1>], 
	[<ore:blockEndSteel>, <ore:blockEndSteel>, <ore:blockOsmiridium>, <ore:ingotCrystalMatrix>, <avaritia:cosmic_meatballs>, <ore:ingotCrystalMatrix>, <ore:blockManyullyn>, <ore:blockVibrantAlloy>, <ore:blockVibrantAlloy>], 
	[<actuallyadditions:item_crystal_empowered:2>, <extrautils2:suncrystal>, <ore:eternalLifeEssence>, <avaritia:ultimate_stew>, <ore:netherStar>, <avaritia:ultimate_stew>, <ore:eternalLifeEssence>, <extrautils2:suncrystal>, <actuallyadditions:item_crystal_empowered:2>], 
	[<mekanism:reinforcedplasticblock:15>, <mekanism:reinforcedplasticblock:15>, <ore:blockMirion>, <ore:ingotCrystalMatrix>, <avaritia:cosmic_meatballs>, <ore:ingotCrystalMatrix>, <ore:blockOsmiridium>, <extendedcrafting:storage:7>, <extendedcrafting:storage:7>], 
	[<actuallyadditions:item_crystal_empowered:3>, <ore:gemBoronNitride>, <ore:gemBoronNitride>, <ore:blockManyullyn>, <ore:eternalLifeEssence>, <ore:blockOsgloglas>, <ore:gemBoronNitride>, <ore:gemBoronNitride>, <actuallyadditions:item_crystal_empowered:1>], 
	[<ore:ingotExtreme>, <ore:blockCosmicNeutronium>, <ore:gemBoronNitride>, <ore:blockSignalum>, <extrautils2:suncrystal>, <ore:blockLumium>, <ore:gemBoronNitride>, <ore:blockCosmicNeutronium>, <ore:ingotExtreme>], 
	[<ore:ingotExtreme>, <ore:ingotExtreme>, <actuallyadditions:item_crystal_empowered:5>, <ore:blockSignalum>, <actuallyadditions:item_crystal_empowered:4>, <ore:blockLumium>, <actuallyadditions:item_crystal_empowered:5>, <ore:ingotExtreme>, <ore:ingotExtreme>]
]);

//Infinity Block
recipes.remove(infigb, false);


mods.extendedcrafting.TableCrafting.addShaped(infigb,
[[<ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>], 
 [<ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>], 
 [<ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>]]);


//Infinity Ingot
mods.avaritia.ExtremeCrafting.remove(infig);

mods.extendedcrafting.TableCrafting.addShaped(0, infig, [

    [null, null, null, null, null, null, null, null, null], 

    [<ore:alloyUltimate>, <ore:alloyUltimate>, <ore:alloyUltimate>, <ore:ingotOsmiridium>, <draconicevolution:draconium_capacitor:1>, <ore:ingotMirion>, <ore:alloyUltimate>, <ore:alloyUltimate>, <ore:alloyUltimate>], 

    [<ore:nuggetUnstable>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotRefinedObsidian>, <ore:ingotVoid>, <ore:ingotRefinedGlowstone>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:nuggetUnstable>], 

    [<ore:ingotCosmicNeutronium>, <ore:ingotRefinedGlowstone>, <ore:ingotCrystalMatrix>, <ore:ingotCrystalMatrix>, <avaritia:resource:5>, <ore:ingotCrystalMatrix>, <ore:ingotCrystalMatrix>, <ore:ingotRefinedObsidian>, <ore:ingotCosmicNeutronium>], 

    [<industrialforegoing:pink_slime_ingot>, <ore:ingotCrystalMatrix>, <nuclearcraft:smore>, <nuclearcraft:moresmore>, <nuclearcraft:foursmore>, <nuclearcraft:moresmore>, <nuclearcraft:smore>, <ore:ingotCrystalMatrix>, <industrialforegoing:pink_slime_ingot>], 

    [<ore:ingotCosmicNeutronium>, <ore:ingotRefinedObsidian>, <ore:ingotCrystalMatrix>, <ore:ingotCrystalMatrix>, <avaritia:resource:5>, <ore:ingotCrystalMatrix>, <ore:ingotCrystalMatrix>, <ore:ingotRefinedGlowstone>, <ore:ingotCosmicNeutronium>], 

    [<ore:nuggetUnstable>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotRefinedGlowstone>, <ore:ingotVoid>, <ore:ingotRefinedObsidian>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:nuggetUnstable>], 

    [<ore:alloyUltimate>, <ore:alloyUltimate>, <ore:alloyUltimate>, <ore:ingotMirion>, <avaritia:resource:5>, <ore:ingotOsmiridium>, <ore:alloyUltimate>, <ore:alloyUltimate>, <ore:alloyUltimate>], 

    [null, null, null, null, null, null, null, null, null]

]);


//Infinity Helmet
mods.avaritia.ExtremeCrafting.remove(infhe);

mods.extendedcrafting.TableCrafting.addShaped(infhe,
   [[null, null, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, null, null], 
	[null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null], 
	[null, <ore:ingotCosmicNeutronium>, null, <avaritia:resource:5>, <ore:ingotInfinity>, <avaritia:resource:5>, null, <ore:ingotCosmicNeutronium>, null], 
	[null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null], 
	[null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null], 
	[null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, null, <ore:ingotInfinity>, null, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null]]);


//Infinity Chestplate
mods.avaritia.ExtremeCrafting.remove(infbp);

mods.extendedcrafting.TableCrafting.addShaped(infbp,
   [[null, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, null, null, null, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, null], 
	[<ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, null, null, null, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, null, null, null, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>], 
	[null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null], 
	[null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:blockCrystalMatrix>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null], 
	[null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null], 
	[null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null], 
	[null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null], 
	[null, null, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, null, null]]);


//Infinity Leggings
mods.avaritia.ExtremeCrafting.remove(infle);

mods.extendedcrafting.TableCrafting.addShaped(infle,
   [[<ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <avaritia:resource:5>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <avaritia:resource:5>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null, null, null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <ore:blockCrystalMatrix>, <ore:ingotCosmicNeutronium>, null, null, null, <ore:ingotCosmicNeutronium>, <ore:blockCrystalMatrix>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null, null, null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null, null, null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null, null, null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, null, null, null, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>]]);


//Infinity Boots
mods.avaritia.ExtremeCrafting.remove(infbo);


mods.extendedcrafting.TableCrafting.addShaped(infbo,
   [[null, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, null, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, null], 
	[null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null], 
	[null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null], 
	[<ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null, <ore:ingotCosmicNeutronium>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, null, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null]]);


//Sword Of The Cosmos
mods.avaritia.ExtremeCrafting.remove(swooc);


mods.extendedcrafting.TableCrafting.addShaped(swooc,
   [[null, null, null, null, null, null, null, <ore:ingotInfinity>, <ore:ingotInfinity>], 
	[null, null, null, null, null, null, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>], 
	[null, null, null, null, null, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, null], 
	[null, null, null, null, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, null, null], 
	[null, <ore:ingotCrystalMatrix>, null, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, null, null, null], 
	[null, null, <ore:ingotCrystalMatrix>, <ore:ingotInfinity>, <ore:ingotInfinity>, null, null, null, null], 
	[null, null, <ore:ingotCosmicNeutronium>, <ore:ingotCrystalMatrix>, null, null, null, null, null], 
	[null, <ore:ingotCosmicNeutronium>, null, null, <ore:ingotCrystalMatrix>, null, null, null, null], 
	[<avaritia:resource:5>, null, null, null, null, null, null, null, null]]);


//Longbow Of The Heavens
mods.avaritia.ExtremeCrafting.remove(lonbh);


mods.extendedcrafting.TableCrafting.addShaped(lonbh,
   [[null, null, null, <ore:ingotInfinity>, <ore:ingotInfinity>, null, null, null, null], 
	[null, null, <ore:ingotInfinity>, null, <ore:wool>, null, null, null, null], 
	[null, <ore:ingotInfinity>, null, null, <ore:wool>, null, null, null, null], 
	[<ore:ingotInfinity>, null, null, null, <ore:wool>, null, null, null, null], 
	[<ore:blockCrystalMatrix>, null, null, null, <ore:wool>, null, null, null, null], 
	[<ore:ingotInfinity>, null, null, null, <ore:wool>, null, null, null, null], 
	[null, <ore:ingotInfinity>, null, null, <ore:wool>, null, null, null, null], 
	[null, null, <ore:ingotInfinity>, null, <ore:wool>, null, null, null, null], 
	[null, null, null, <ore:ingotInfinity>, <ore:ingotInfinity>, null, null, null, null]]);


//World Breaker
mods.avaritia.ExtremeCrafting.remove(worldb);


mods.extendedcrafting.TableCrafting.addShaped(worldb,
   [[null, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, null], 
	[<ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:blockCrystalMatrix>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>], 
	[<ore:ingotInfinity>, <ore:ingotInfinity>, null, null, <ore:ingotCosmicNeutronium>, null, null, <ore:ingotInfinity>, <ore:ingotInfinity>], 
	[null, null, null, null, <ore:ingotCosmicNeutronium>, null, null, null, null], 
	[null, null, null, null, <ore:ingotCosmicNeutronium>, null, null, null, null], 
	[null, null, null, null, <ore:ingotCosmicNeutronium>, null, null, null, null], 
	[null, null, null, null, <ore:ingotCosmicNeutronium>, null, null, null, null], 
	[null, null, null, null, <ore:ingotCosmicNeutronium>, null, null, null, null], 
	[null, null, null, null, <ore:ingotCosmicNeutronium>, null, null, null, null]]);


//Planet Eater
mods.avaritia.ExtremeCrafting.remove(plnte);


mods.extendedcrafting.TableCrafting.addShaped(plnte,
   [[null, null, null, null, null, null, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>], 
	[null, null, null, null, null, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:blockInfinity>, <ore:ingotInfinity>], 
	[null, null, null, null, null, null, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>], 
	[null, null, null, null, null, <ore:ingotCosmicNeutronium>, null, <ore:ingotInfinity>, null], 
	[null, null, null, null, <ore:ingotCosmicNeutronium>, null, null, null, null], 
	[null, null, null, <ore:ingotCosmicNeutronium>, null, null, null, null, null], 
	[null, null, <ore:ingotCosmicNeutronium>, null, null, null, null, null, null], 
	[null, <ore:ingotCosmicNeutronium>, null, null, null, null, null, null, null], 
	[<ore:ingotCosmicNeutronium>, null, null, null, null, null, null, null, null]]);


//Nature's Ruin
mods.avaritia.ExtremeCrafting.remove(nturr);


mods.extendedcrafting.TableCrafting.addShaped(nturr,
   [[null, null, null, <ore:ingotInfinity>, null, null, null, null, null], 
	[null, null, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, null, null], 
	[null, null, null, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, null, null], 
	[null, null, null, null, null, <ore:ingotInfinity>, <ore:ingotCosmicNeutronium>, null, null], 
	[null, null, null, null, null, null, <ore:ingotCosmicNeutronium>, null, null], 
	[null, null, null, null, null, null, <ore:ingotCosmicNeutronium>, null, null], 
	[null, null, null, null, null, null, <ore:ingotCosmicNeutronium>, null, null], 
	[null, null, null, null, null, null, <ore:ingotCosmicNeutronium>, null, null], 
	[null, null, null, null, null, null, <ore:ingotCosmicNeutronium>, null, null]]);


//Hoe Of The Green Earth
mods.avaritia.ExtremeCrafting.remove(infho);


mods.extendedcrafting.TableCrafting.addShaped(infho,
   [[null, null, null, null, null, <ore:ingotCosmicNeutronium>, null, null, null], 
	[null, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, null, null], 
	[<ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, null, null], 
	[<ore:ingotInfinity>, null, null, null, null, <ore:ingotInfinity>, <ore:ingotInfinity>, null, null], 
	[null, null, null, null, null, <ore:ingotCosmicNeutronium>, null, null, null], 
	[null, null, null, null, null, <ore:ingotCosmicNeutronium>, null, null, null], 
	[null, null, null, null, null, <ore:ingotCosmicNeutronium>, null, null, null], 
	[null, null, null, null, null, <ore:ingotCosmicNeutronium>, null, null, null], 
	[null, null, null, null, null, <ore:ingotCosmicNeutronium>, null, null, null]]);


//Skullfire Sword
mods.avaritia.ExtremeCrafting.remove(skulf);


mods.extendedcrafting.TableCrafting.addShaped(skulf,
   [[null, null, null, null, null, null, null, <ore:ingotCrystalMatrix>, <ore:itemBlazePowder>], 
	[null, null, null, null, null, null, <ore:ingotCrystalMatrix>, <ore:itemBlazePowder>, <ore:ingotCrystalMatrix>], 
	[null, null, null, null, null, <ore:ingotCrystalMatrix>, <ore:itemBlazePowder>, <ore:ingotCrystalMatrix>, null], 
	[null, null, null, null, <ore:ingotCrystalMatrix>, <ore:itemBlazePowder>, <ore:ingotCrystalMatrix>, null, null], 
	[null, <ore:bone>, null, <ore:ingotCrystalMatrix>, <ore:itemBlazePowder>, <ore:ingotCrystalMatrix>, null, null, null], 
	[null, null, <ore:bone>, <ore:itemBlazePowder>, <ore:ingotCrystalMatrix>, null, null, null, null], 
	[null, null, <ore:logWood>, <ore:bone>, null, null, null, null, null], 
	[null, <ore:logWood>, null, null, <ore:bone>, null, null, null, null], 
	[<ore:netherStar>, null, null, null, null, null, null, null, null]]);


recipes.addShaped("Iron Ring", iring,
 [[null,iron,null],
  [niron,null,niron],
  [null,niron,null]]);








